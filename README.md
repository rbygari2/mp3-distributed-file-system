# CS 425 Fall 2022 MP3 - – Simple Distributed File System

Group 34 - Arpitha Raghunandan (arpitha2) and Ramya Bygari (rbygari2)

(Built on top of the reference MP2 solution posted on the course website)

## Running a Node

In order to start up a node, run these commands from the mp2 directory:
```
make
./bin/exec <machine_id> <is_introducer>
OR: ./bin/exec <machine_id> <is_introducer> <introducer_ip>
OR: ./bin/exec <machine_id> <is_introducer> <ping_rate> <drop_threshold> <loss_rate>
```
Once a node starts up, you can input the following commands:
- LIST_MEM = print this node's member list
- LIST_SELF = print this node's id
- JOIN = join the group
- LEAVE = leave the group
- WAIT = wait 1 second, used for manual tests
- STOP = kill this node
- PUT localfilename sdfsfilename = insert the original file localfilename / update it as sdfsfilename
- GET sdfsfilename localfilename = fetches latest version of sdfsfilename to local file directory as localfilename
- GET-VERSIONS sdfsfilename NUM_VERSIONS localfilename = fetches NUM_VERSIONS versions of sdfsfilename to local file directory as localfilename (minimum = max (num of replicas, NUM_VERSIONS), maximum = min (NUM_VERSIONS, 5))
- LS sdfsfilename = list of all machine (VM) addresses where sdfsfilename is currently being stored
- STORE = list all files currently being stored at this machine

## Using MP2 for failure detection

The ring membership protocol used for failure detection is continued to be used for this MP as well. 

## Using MP1 for logging

In order to start up a node on machine i, run these commands from the mp2 directory:
```
make
./bin/exec <machine_id> <is_introducer> > /home/shared/logs/mp2/machine.i.log
OR: ./bin/exec <machine_id> <is_introducer> <introducer_ip> > /home/shared/logs/mp2/machine.i.log
OR: ./bin/exec <machine_id> <is_introducer> <ping_rate> <drop_threshold> <loss_rate> > /home/shared/logs/mp2/machine.i.log
```

All logs have a time stamp attached so you can track how long operations take.
