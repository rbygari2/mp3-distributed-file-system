#include <string>

/**
  For constants that should be used throughout the MP
*/
namespace cs425 {
  /**
    agreed port between client and server
  */
  const unsigned int PORT = 4251;
  const unsigned int NUM_MONITORS = 3;
  // std::string INTRODUCER_ADDRESS = "172.22.156.112";
  const unsigned int NUM_REPLICAS = 5;
  const int R = 2;
  const int W = 4;
}