#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include "member.hpp"

#include <string>
#include <vector>

enum class MessageType {
    Join = 1,
    JoinAck = 2,
    Introduce = 3,
    Ping = 4,
    Ack = 5,
    Leave = 6,
    Replicate = 7,
    ReplicateAck = 8,
    AddNewFile = 9,
    Delete = 10,
    DeleteAck = 11,
    DeleteFile = 12,
    Get = 13,
    GetResponse = 14,
    Write = 15,
    WriteAck = 16,
};

class Message {
public:
    Message(MessageType type, std::vector<Member> members, std::string file_info);

    static std::string Serialize(const Message& message);
    static Message Deserialize(const std::string& serialized_message);

    const MessageType& GetType() const;
    const std::vector<Member>& GetMembers() const;
    const std::string& GetFileInfo() const;

    static std::vector<std::string> Split(const std::string& str, char delimiter);

private:
    
    MessageType type_;
    std::vector<Member> members_;
    std::string file_info_;
};

#endif