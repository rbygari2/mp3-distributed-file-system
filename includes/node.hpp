#ifndef NODE_HPP
#define NODE_HPP

#include "member.hpp"

#include <atomic>
#include <chrono>
#include <mutex>
#include <vector>
#include <set>
#include <map>

class Node {
public:
    Node(int id, bool is_introducer, int ping_rate, int drop_threshold, double loss_rate, std::string introducer_address);

    void Start();

    void HandleUserCommands();
    void PingSuccessors();
    void MessageHandler();
    void MaintainReplicas();

    void HandleJoin(const Member& new_member);
    void HandleJoinAck(const std::vector<Member>& existing_members);
    void HandleIntroduce(const Member& new_member);
    void HandlePing(const std::string& source);
    void HandleAck(const Member& acking_member);
    void HandleLeave(const Member& leaving_member);
    void HandlePut(const std::string& localfilename, const std::string& sdfsfilename);
    void HandleReplicate(const std::string& file_info);
    void HandleAddFile(const std::vector<Member>& replica_members, const std::string &file_name);
    void HandleStore();
    void HandleList(const std::string& sdfsfilename);
    void HandleFileDelete(const std::string& sdfsfilename);
    void HandleDelete(const std::string& sdfsfilename);
    void HandleMemberDeleteFile(const std::vector<Member>& replica_members, const std::string& file_name);
    void HandleGet(const std::string& sdfsfilename);
    void HandleGetRequest(const std::string& file_name, const std::string& source);
    void HandleGetResponse(const std::string& sdfsfileinfo, const std::string& source);
    void HandleWrite(const std::string& source);
    void HandleWriteAck(const std::string& source);


private:
    void LogMemberJoinIntroducer(const Member& member);
    void LogMemberJoinAck(const Member& member);
    void LogMemberJoin(const Member& member);
    void LogMemberLeave(const Member& member);
    void LogMemberLeaveForwarded(const Member& member);
    void LogMemberFailure(const Member& member);
    void LogPut(const std::vector<Member>& replicas, const std::string& sdfsfile);
    void LogReplicate(const std::string& file_name);
    void LogMemberAddFile(const std::string& file_name);
    void LogFileDelete(const std::vector<Member>& replicas, const std::string& file_name);
    void LogDelete(const std::string& file_name);
    void LogMemberDeleteFile(const std::string& file_name);
    void LogGet(const std::vector<Member>& members, const std::string& file_name);
    void LogGetRequest(const std::string& file_name);
    void LogGetResponse(const std::string& file_name);
    void LogMisc(const std::string& tag, const std::string& message);
    void PrintMembers();
    void PrintSelf();
    double GetElapsedSeconds();
    void DebugPrintMembers();
    std::vector<Member> GetSuccessors();
    std::map<std::string, std::vector<Member>> GetFileToMembersMap();
    void ReplicateFilesOnLeave(const std::string& file_name, const Member& member_without_file);

    int id_;
    bool is_introducer_;
    const int ping_rate_;
    const int drop_threshold_;
    const double loss_rate_;
    std::string introducer_address_;
    std::vector<Member> members_;
    std::set<std::string> files_;
    std::atomic_uint ring_position_ = 0;
    std::mutex mutex_;
    Member self_;
    std::atomic_bool is_member_ = false;
    int current_num_read_ = 0;
    std::string current_local_file_name_ = "";
    std::map<std::string, std::string> current_sdfs_version_file_info_;
    int requested_num_versions_ = 1;
    std::string write_file_info_ = "";
    std::string write_sdfsfilename_ = "";
    std::vector<Member> write_replica_members_;
    int current_num_write_acks_ = 0;
    double get_start_time_ = 0;
    double put_start_time_ = 0;

    std::chrono::time_point<std::chrono::steady_clock> start_time_;
};

#endif