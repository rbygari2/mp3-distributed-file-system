#ifndef MEMBER_HPP
#define MEMBER_HPP

#include <string>

struct Member {
    std::string id = "0"; // id + timestamp + address
    std::string address = "0.0.0.0";
    int pings_dropped = 0;
    std::string files_present = "";

    bool operator==(const Member& rhs) { return id == rhs.id && address == rhs.address; }
};

#endif