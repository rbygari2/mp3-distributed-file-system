#include "node.hpp"

#include <iostream>
#include <string>

/**
  entrypoint for MP2. Takes in two arguments:

  @param machine_id unique id for this machine
  @param is_introducer whether this machine is the interoducer
*/
int main(int argc, char* argv[]) {
    if (argc != 3 && argc != 6 && argc != 4) {
        std::cout << "Usage: ./bin/exec <machine_id> <is_introducer>" << std::endl;
        std::cout << "OR: ./bin/exec <machine_id> <is_introducer> <introducer_ip>" << std::endl;
        std::cout << "OR: ./bin/exec <machine_id> <is_introducer> <ping_rate> <drop_threshold> <loss_rate>" << std::endl;
        return 1;
    }

    int machine_id = std::stoi(argv[1]);
    bool is_introducer = std::string(argv[2]) == "true";

    std::string introducer_address = "172.22.156.112";

    if (argc == 4) {
      introducer_address = std::string(argv[3]);
    }

    int ping_rate = 1;
    int drop_threshold = 3;
    double loss_rate = 0.0;
    if (argc == 6) {
      ping_rate = std::stoi(argv[3]);
      drop_threshold = std::stoi(argv[4]);
      loss_rate = std::stod(argv[5]);
    }

    Node machine(machine_id, is_introducer, ping_rate, drop_threshold, loss_rate, introducer_address);
    machine.Start();

    return 0;
}