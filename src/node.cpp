#include "node.hpp"

#include "constants.hpp"
#include "message.hpp"
#include "socket.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <optional>
#include <thread>
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <set>
#include <map>
#include <fstream>
#include <filesystem>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>

Node::Node(int id, bool is_introducer, int ping_rate, int drop_threshold, double loss_rate, std::string introducer_address) :
    id_{id}, is_introducer_{is_introducer}, ping_rate_{ping_rate}, drop_threshold_{drop_threshold}, loss_rate_{loss_rate}, introducer_address_{introducer_address} {}

void Node::Start() {
    start_time_ = std::chrono::steady_clock::now();

    std::thread ping_thread(&Node::PingSuccessors, this);
    ping_thread.detach();
    std::thread process_thread(&Node::MessageHandler, this);
    process_thread.detach();
    std::thread maintain_replicas_thread(&Node::MaintainReplicas, this);
    maintain_replicas_thread.detach();

    Node::HandleUserCommands();
}

void Node::HandleUserCommands() {
    while (true) {
        LogMisc("INFO", "Type any of the following: LIST_MEM/LIST_SELF/JOIN/LEAVE/WAIT/STOP");
        std::string input;
        std::cin >> input;

        if (input == "LIST_MEM") {
            PrintMembers();
        } else if (input == "LIST_SELF") {
            PrintSelf();
        } else if (input == "JOIN") {
            if (is_member_) {
                LogMisc("ERROR", "Node is already in group, ignoring JOIN");
                continue;
            }

            // Don't know IP address until JoinAck received, so start w/ ID only
            time_t time_since_epoch = time(0);
            std::string compound_id = std::to_string(id_) + "-" + std::to_string(time_since_epoch);
            Member self_partial{compound_id, "0.0.0.0", 0};

            if (is_introducer_) {
                self_.id = compound_id + "-" + introducer_address_;
                self_.address = introducer_address_;
                HandleJoin(self_);
                is_member_ = true;
                continue;
            }

            Message outbound_message{MessageType::Join, {self_partial}, ""};
            std::string serialized_outbound_message = Message::Serialize(outbound_message);
            Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, introducer_address_, loss_rate_);

        } else if (input == "LEAVE") {
            if (!is_member_) {
                LogMisc("ERROR", "Node is not in group, ignoring LEAVE");
                continue;
            }

            Message outbound_message{MessageType::Leave, {self_}, ""};
            std::string serialized_outbound_message = Message::Serialize(outbound_message);
            
            for (const Member& successor : GetSuccessors()) {
                Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
            }

            mutex_.lock();

            members_.clear();
            files_.clear();

            mutex_.unlock();

            is_member_ = false;
            is_introducer_ = false;

            LogMemberLeave(self_);
        } else if (input == "WAIT") {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } else if (input == "PUT") {
            // std::cout << "BEGIN PUT " << std::endl;
            std::string file_names, localfilename, sdfsfilename;
            std::cin >> localfilename;
            std::cin >> sdfsfilename;
            // std::vector<std::string> file_name = Message::Split(file_names, ' ');
            // localfilename = file_name[0];
            // sdfsfilename = file_name[1];
            // std::cout << "localfilename: " << localfilename << std::endl;
            // std::cout << "sdfsfilename: " << sdfsfilename << std::endl;
            put_start_time_ = GetElapsedSeconds();
            HandlePut(localfilename, sdfsfilename);
        } else if (input == "STORE"){
            HandleStore();
        } else if (input == "LS"){
            std::string sdfsfilename;
            std::cin >> sdfsfilename;
            HandleList(sdfsfilename);
        } else if (input == "DELETE") {
            std::string sdfsfilename;
            std::cin >> sdfsfilename;
            HandleFileDelete(sdfsfilename);
        } else if (input == "GET") {
            std::string sdfsfilename;
            std::cin >> sdfsfilename;
            std::cin >> current_local_file_name_;
            get_start_time_ = GetElapsedSeconds();
            HandleGet(sdfsfilename);
        } else if (input == "GET-VERSIONS") {
            std::string sdfsfilename;
            std::cin >> sdfsfilename;
            std::cin >> requested_num_versions_;
            std::cin >> current_local_file_name_;
            // std::cout << "sdfsfilename: " << sdfsfilename << std::endl;
            // std::cout << "requested_num_versions_: " << requested_num_versions_ << std::endl;
            // std::cout << "current_local_file_name_: " << current_local_file_name_ << std::endl;
            HandleGet(sdfsfilename);
        } else if (std::cin.eof() || input == "STOP"){
            break;
        } else {
            LogMisc("ERROR", "Command not recognized, try again");
        }
    }
}

void Node::PingSuccessors() {
    while (true) {
        if (is_member_) {
            for (const Member& member : GetSuccessors()) {
                mutex_.lock();

                std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), member);

                int num_dropped = (++(it->pings_dropped));

                mutex_.unlock();

                if (num_dropped > drop_threshold_) {
                    HandleLeave(member);
                    LogMemberFailure(member);
                } else {
                    Message outbound_message{MessageType::Ping, {}, ""};
                    std::string serialized_outbound_message = Message::Serialize(outbound_message);
                    Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, member.address, loss_rate_);
                }
            }
        }

        std::this_thread::sleep_for(std::chrono::seconds(ping_rate_));
    }
}

void Node::MessageHandler() {
    std::optional<Socket> opt_socket = Socket::ConstructReceiver(cs425::PORT);
    Socket socket = std::move(*opt_socket);

    while (true) {
        std::pair<std::string, std::string> result = socket.ReceiveMessage();
        std::string serialized_received_message = result.first;
        std::string sender = result.second;
        
        // No sender address found - skip
        if (sender == "") {
            continue;
        }

        Message received_message = Message::Deserialize(serialized_received_message);

        switch (received_message.GetType()) {
            case MessageType::Join: {
                // Only Introducer can process Join messages
                if (is_introducer_ && !received_message.GetMembers().empty()) {              
                    Member new_member = received_message.GetMembers().at(0);
                    // Attach address to IP
                    new_member.id += "-" + sender;
                    new_member.address = sender;

                    std::string new_member_id = Message::Split(new_member.id, '-')[0];
                    LogMisc("HIHI", "ID: " + std::to_string(id_) + " new_member.id: " + new_member_id + " new_member.address: " + new_member.address);
                    // If this is Introducer Node or Introducer is present, can introduce to everyone
                    if (std::stoi(new_member_id) == id_ || is_member_) {
                        HandleJoin(new_member);
                    } else {
                        LogMisc("ERROR", "JOIN rejected, introducer not in group yet");
                    }                    
                }

                break;
            }
            case MessageType::JoinAck: {
                HandleJoinAck(received_message.GetMembers());

                break;
            }
            case MessageType::Introduce: {
                if (is_member_ && !received_message.GetMembers().empty()) {
                    Member new_member = received_message.GetMembers().at(0);
                    HandleIntroduce(new_member);
                }
                
                break;
            }
            case MessageType::Ping: {
                if (is_member_) {
                    HandlePing(sender);
                }

                break;
            }
            case MessageType::Ack: {
                if (is_member_ && !received_message.GetMembers().empty()) {
                    Member acking_member = received_message.GetMembers().at(0);
                    HandleAck(acking_member);
                }

                break;
            }
            case MessageType::Leave: {
                if (is_member_ && !received_message.GetMembers().empty()) {
                    Member leaving_member = received_message.GetMembers().at(0);
                    HandleLeave(leaving_member);
                }

                break;
            }
            case MessageType::Replicate: {
                if (is_member_) {
                    HandleReplicate(received_message.GetFileInfo());
                }

                break;
            }
            case MessageType::AddNewFile: {
                if (is_member_) {
                    HandleAddFile(received_message.GetMembers(), received_message.GetFileInfo());
                }

                break;
            }
            case MessageType::Delete: {
                if (is_member_) {
                    HandleDelete(received_message.GetFileInfo());
                }

                break;
            }
            case MessageType::DeleteFile: {
                if (is_member_) {
                    HandleMemberDeleteFile(received_message.GetMembers(), received_message.GetFileInfo());
                }

                break;
            }
            case MessageType::Get: {
                if (is_member_) {
                    HandleGetRequest(received_message.GetFileInfo(), sender);
                }

                break;
            }
            case MessageType::GetResponse: {
                if (is_member_) {
                    HandleGetResponse(received_message.GetFileInfo(), sender);
                }

                break;
            }
            case MessageType::Write: {
                if (is_member_) {
                    HandleWrite(sender);
                }

                break;
            }
            case MessageType::WriteAck: {
                if (is_member_) {
                    HandleWriteAck(sender);
                }

                break;
            }
            default: {
                LogMisc("ERROR", "Unknown MessageType '" + std::to_string(static_cast<unsigned int>(received_message.GetType())) + "' found, exiting message handler");
                exit(1);
            }
        }
    }
}

void Node::MaintainReplicas() {
    while (true) {
        if (is_member_) {
            //Get file to members map
            std::map<std::string, std::vector<Member>> file_to_members_map = GetFileToMembersMap();

            for (auto ftom: file_to_members_map) {
                std::string file = ftom.first;
                std::vector<Member> members = ftom.second;
                // If file is not replicated enough, replicate
                if (members.size() < cs425::NUM_REPLICAS && files_.find(file) != files_.end()) {
                    double replicate_start_time = GetElapsedSeconds();
                    bool found = false;
                    Member member_without_file;
                    int num_members = members_.size();
                    while (!found) {
                        int random_index = rand() % num_members;
                        member_without_file = members_[random_index];
                        if (std::find(members.begin(), members.end(), member_without_file) == members.end()) {
                            found = true;
                        }
                        if (members_.size() < cs425::NUM_REPLICAS) {
                            break;
                        }
                    }
                    if (found) {
                        ReplicateFilesOnLeave(file, member_without_file);
                        double replicate_end_time = GetElapsedSeconds();
                        LogMisc("PERF REPLICATE", "REPLICATE took " + std::to_string(replicate_end_time - replicate_start_time) + " seconds");
                    }                        
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::seconds(ping_rate_));
    }
}

void Node::HandleJoin(const Member& new_member) {
    Message outbound_message{MessageType::Introduce, {new_member}, ""};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);

    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    mutex_.lock();

    // Because GetSuccessors won't ping the Introducer, introducer manually adds the new
    // node to its own list
    members_.push_back(new_member);
    outbound_message = {MessageType::JoinAck, members_, ""};

    mutex_.unlock();

    serialized_outbound_message = Message::Serialize(outbound_message);
    Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, new_member.address, loss_rate_);

    LogMemberJoinIntroducer(new_member);
}

void Node::HandleJoinAck(const std::vector<Member>& existing_members) {
    mutex_.lock();

    // New node's membership list is set to introducer's. All new nodes are at back of list
    members_ = existing_members;
    ring_position_ = members_.size() - 1;
    // This self member variable is a bit sloppy, but used in a few spots for a node to reference itself
    self_ = members_.at(ring_position_);

    mutex_.unlock();

    // Only say you are a member after getting a JoinAck
    is_member_ = true;
    is_introducer_ = true;

    LogMemberJoinAck(self_);
}

void Node::HandleIntroduce(const Member& new_member) {
    mutex_.lock();

    std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), new_member);

    // If you have already received the new node, drop the message
    if (it != members_.end()) {
        mutex_.unlock();
        return;
    }

    // Otherwise, add the new node to the back of membership list
    members_.push_back(new_member);

    mutex_.unlock();

    // Forward to successors
    Message outbound_message{MessageType::Introduce, {new_member}, ""};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    LogMemberJoin(new_member);
}

void Node::HandlePing(const std::string& source) {
    Message outbound_message{MessageType::Ack, {self_}, ""};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, source, loss_rate_);
}

void Node::HandleAck(const Member& acking_member) {
    mutex_.lock();

    std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), acking_member);

    if (it == members_.end()) {
        mutex_.unlock();
        return;
    }

    it->pings_dropped = 0;

    mutex_.unlock();
}

void Node::HandleLeave(const Member& leaving_member) {
    mutex_.lock();

    // Find index of where leaving node is
    int found_index = -1;
    for (size_t index = 0; index < members_.size(); ++index) {
        if (members_[index] == leaving_member) {
            found_index = index;
            break;
        }
    }

    // If you have already removed the leaving node, drop the message
    if (found_index == -1) {
        mutex_.unlock();
        return;
    }

    // Otherwise, remove said node
    members_.erase(members_.begin() + found_index);

    // Check if erased node was behind you, if so decrement your ring position
    if (ring_position_ > static_cast<unsigned int>(found_index)) {
        --ring_position_;
    }

    mutex_.unlock();

    // Forward to successors
    Message outbound_message{MessageType::Leave, {leaving_member}, ""};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    LogMemberLeaveForwarded(leaving_member);
}

void Node::HandlePut(const std::string& localfilename, const std::string& sdfsfilename) {
    // std::cout<<"BEGIN HANDLE PUT"<<std::endl;
    int num_members = members_.size();
    // int num_replicas = 4;
    std::set<int> replicas;

    const auto clock_time = std::chrono::system_clock::now();
	int time_stamp = std::chrono::duration_cast<std::chrono::seconds>(clock_time.time_since_epoch()).count();

    std::string file_info = sdfsfilename + "_" + std::to_string(time_stamp) + "\n";

    // Get file content
    std::ifstream localfile(localfilename);
    if (localfile.is_open()) {
        std::string line;
        while (std::getline(localfile, line)) {
            file_info += line + "\n";
        }
        localfile.close();
    } else {
        LogMisc("ERROR", "Could not open file '" + localfilename + "\n");
        return;
    }

    // If file already exists, replicate on machines that have it already
    std::map<std::string, std::vector<Member>> file_to_members_map = GetFileToMembersMap();
    std::vector<Member> members = file_to_members_map[sdfsfilename];

    std::vector<Member> replicas_members; 
    
    if (members.size() > 0) {
        for (const Member& member : members) {
            replicas_members.push_back(member);
        }
    }

    else {
        // Generate 4 random numbers between 0 and num_members
        while ((unsigned int)replicas.size() < cs425::NUM_REPLICAS) {
            int random_index = rand() % num_members;
            replicas.insert(random_index);
            // std::cout<<"Random index in PUT: "<<random_index<<std::endl;
        }

        for (int replica_index : replicas) {
            replicas_members.push_back(members_[replica_index]);
        }
    }

    // TODO: send write
    Message write_outbound_message{MessageType::Write, {}, ""};
    std::string write_serialized_outbound_message = Message::Serialize(write_outbound_message);
    for (const Member& member : replicas_members) {
        Socket::TrySendMessage(write_serialized_outbound_message, cs425::PORT, member.address, loss_rate_);
    }

    write_file_info_ = file_info;
    write_replica_members_ = replicas_members;
    write_sdfsfilename_ = sdfsfilename;

    LogMisc("INFO", "Waiting for W acks");
}

void Node::HandleReplicate(const std::string& file_info) {
    // std::cout<<"BEGIN HANDLE REPLICATE "<< source <<std::endl;
    std::vector<std::string> lines = Message::Split(file_info, '\n');
    std::string file_name_with_version = lines[0];
    std::string file_name = Message::Split(file_name_with_version, '_')[0];
    // std::cout << "File name: " << file_name << std::endl;
    // std::cout << "File name with version: " << file_name_with_version << std::endl;
    std::string file_content = "";
    lines.erase(lines.begin());
    for (std::string line : lines) {
        file_content += line + "\n";
    }

    // Write file to local disk
    DIR *dr;
    // struct dirent *en;
    dr = opendir(file_name.c_str());
    if (!dr) {
        // file does not exist
        if (mkdir(file_name.c_str(), 0777) == -1) {
            LogMisc("ERROR", "Could not create directory '" + file_name + "'\n");
            return;
        }
    }

    std::ofstream sdfsfile ("./" + file_name + "/" + file_name_with_version);
    if (sdfsfile.is_open()) {
        sdfsfile << file_content;
        sdfsfile.close();
    }
    else {
        LogMisc("ERROR", "Could not create replica '" + file_name_with_version + "'\n");
        return;
    }

    self_.files_present += file_name + ",";
    files_.insert(file_name);
    // std::cout << "sender: " << source << std::endl;

    LogReplicate(file_name_with_version);
}

void Node::HandleAddFile(const std::vector<Member>& replica_members, const std::string &file_name) {
    mutex_.lock();
 
    int num_replicas = (int)replica_members.size();
    for(int i = 0; i < num_replicas; i++){
        std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), replica_members[i]);
        if (it != members_.end()) {
            if (it->files_present.find(file_name) == std::string::npos) {
                it->files_present += file_name + ",";
            } 
            else {
                mutex_.unlock();
                return;
            }
        }
    }
    mutex_.unlock();

    // Forward to successors
    Message outbound_message{MessageType::AddNewFile, replica_members, file_name};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    LogMemberAddFile(file_name);
}

void Node::HandleStore() {
    std::cout << "---------------------------------------------------------------" << std::endl;
    if (files_.size() == 0) {
        std::cout << "No Files on node " << self_.id << std::endl;
        return;
    }
    std::cout << "Files on node " << self_.id << ":" << std::endl;
    for (std::string file : files_) {
        std::cout << file << std::endl;
    }
}

void Node::HandleList(const std::string& sdfsfilename) {
    std::cout << "---------------------------------------------------------------" << std::endl;

    std::map<std::string, std::vector<Member>> file_to_members_map = GetFileToMembersMap();
    std::vector<Member> members = file_to_members_map[sdfsfilename];
    if (members.size() == 0) {
        std::cout << "File '" << sdfsfilename << "' not found" << std::endl;
        return;
    }
    std::cout << "File " << sdfsfilename << " is present on the following nodes:" << std::endl;
    for (Member member : members) {
        std::cout << member.id << std::endl;
    }    
}

void Node::HandleFileDelete(const std::string& sdfsfilename) {
    std::map<std::string, std::vector<Member>> file_to_members_map = GetFileToMembersMap();
    std::vector<Member> members = file_to_members_map[sdfsfilename];
    if (members.size() == 0) {
        LogMisc("ERROR in DELETE", "File '" + sdfsfilename + "' not found");
        return;
    }

    // Send Delete to each replica
    Message outbound_message{MessageType::Delete, {}, sdfsfilename};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    for (Member member : members) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, member.address, loss_rate_);
    }

    // Forward to successors
    Message deletefile_outbound_message{MessageType::DeleteFile, members, sdfsfilename};
    std::string deletefile_serialized_outbound_message = Message::Serialize(deletefile_outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(deletefile_serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }
    LogFileDelete(members, sdfsfilename);
}

void Node::HandleDelete(const std::string& sdfsfilename) {
    std::string file_name = sdfsfilename.substr(0, sdfsfilename.length() - 1);
    if (files_.find(file_name) == files_.end()) {
        LogMisc("ERROR in DELETE", "File '" + file_name + "' not found");
        return;
    }

    // Delete file from local disk
    std::string file_path = "./" + file_name;
    std::string delete_command = "rm -rf " + file_path;
    system(delete_command.c_str());

    files_.erase(file_name);

    LogDelete(file_name);
}

void Node::HandleMemberDeleteFile(const std::vector<Member>& replica_members, const std::string &file_name) {
    mutex_.lock();
 
    int num_replicas = (int)replica_members.size();
    for(int i = 0; i < num_replicas; i++){
        std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), replica_members[i]);
        size_t pos = it->files_present.find(file_name);
        if (it != members_.end()) {
            if (pos != std::string::npos) {
                it->files_present.erase(pos, file_name.length() + 1);
            } 
            else {
                mutex_.unlock();
                return;
            }
        }
    }
    mutex_.unlock();

    // Forward to successors
    Message outbound_message{MessageType::DeleteFile, replica_members, file_name};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    LogMemberDeleteFile(file_name);
}

void Node::HandleGet(const std::string& sdfsfilename) {
    std::map<std::string, std::vector<Member>> file_to_members_map = GetFileToMembersMap();
    std::vector<Member> members = file_to_members_map[sdfsfilename];
    if (members.size() == 0) {
        LogMisc("ERROR in GET", "File '" + sdfsfilename + "' not found");
        return;
    }

    // Send Get to each replica
    std::string info_req = sdfsfilename + "\n" + std::to_string(requested_num_versions_);
    Message outbound_message{MessageType::Get, {}, info_req};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    int num_requests_sent = 0;
    for (Member member : members) {
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, member.address, loss_rate_);
        num_requests_sent++;
        if (num_requests_sent == cs425::R) {
            break;
        }
    }

    LogGet(members, sdfsfilename);
}

void Node::HandleGetRequest(const std::string& file_name, const std::string& source) {
    std::vector<std::string> info_req = Message::Split(file_name, '\n');
    std::string sdfsfilename = info_req[0];
    int num_versions = std::stoi(info_req[1]);

    if (files_.find(sdfsfilename) == files_.end()) {
        LogMisc("ERROR in GET REQUEST", "File '" + sdfsfilename + "' not found");
        return;
    }

    std::set<std::string> file_names;
    DIR* dirp;
    struct dirent* direntp;
    dirp = opendir(("./" + sdfsfilename).c_str());
    if( dirp != NULL ) {
        for(;;) {
            direntp = readdir( dirp );
            if( direntp == NULL ) break;
            if(strcmp(direntp->d_name, ".")==0 || strcmp(direntp->d_name, "..")==0)
                continue;

            file_names.insert(direntp->d_name);
        }

        closedir( dirp );
    }

    int total_versions = (int)file_names.size();
    int skip_versions = total_versions - num_versions;

    for (std::string file_name: file_names) {
        if (skip_versions > 0) {
            skip_versions--;
            continue;
        }
        std::string file_content = file_name + "\n" + std::to_string(total_versions) + "\n";

        std::ifstream localfile("./" + sdfsfilename + "/" + file_name);
        if (localfile.is_open()) {
            std::string line;
            while (std::getline(localfile, line)) {
                file_content += line + "\n";
            }
            localfile.close();
        } else {
            LogMisc("ERROR", "Could not open file '" + file_name + "\n");
            return;
        }

        Message outbound_message{MessageType::GetResponse, {}, file_content};
        std::string serialized_outbound_message = Message::Serialize(outbound_message);
        Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, source, loss_rate_);
        LogGetRequest(file_name);
    }
}

void Node::HandleGetResponse(const std::string& sdfsfileinfo, const std::string& source) {
    // std::cout << "BEGIN HANDLE GET RESPONSE " << std::endl;
    std::vector<std::string> lines = Message::Split(sdfsfileinfo, '\n');
    std::string file_name_with_version = lines[0];
    int total_versions = std::stoi(lines[1]);

    requested_num_versions_ = std::min(requested_num_versions_, total_versions);

    // std::cout << "File name with version: " << file_name_with_version << std::endl;
    std::string file_content = "";
    lines.erase(lines.begin());
    lines.erase(lines.begin());
    for (std::string line : lines) {
        file_content += line + "\n";
    }

    current_sdfs_version_file_info_[file_name_with_version] = file_content;
    current_num_read_++;

    LogMisc("READ", std::to_string(current_num_read_) + "/" + std::to_string(cs425::R * requested_num_versions_));
    LogMisc("INFO", "Read ACK received from " + source);

    if (current_num_read_ == (cs425::R * requested_num_versions_)) {

        int total_versions = (int)current_sdfs_version_file_info_.size();
        int skip_versions = total_versions - requested_num_versions_;
        int version = 0;

        std::string final_file_content = "";

        for (auto it = current_sdfs_version_file_info_.begin(); it != current_sdfs_version_file_info_.end(); it++) {
            if (skip_versions > 0) {
                skip_versions--;
                continue;
            }

            final_file_content += "VERSION " + std::to_string(version) + "\n\n" + it->second + "\n\n";
            version++;
        }

        std::ofstream localfile ("./" + current_local_file_name_);
        if (localfile.is_open()) {
            localfile << final_file_content;
            localfile.close();
        }
        else {
            LogMisc("ERROR", "Could not create local file '" + current_local_file_name_ + "'\n");
            return;
        }

        LogGetResponse(current_local_file_name_);

        double get_end_time = GetElapsedSeconds();
        LogMisc("PERF GET", "GET took " + std::to_string(get_end_time - get_start_time_) + " seconds");
        get_start_time_ = 0;

        current_num_read_ = 0;
        current_sdfs_version_file_info_.clear();
        requested_num_versions_ = 1;
        current_local_file_name_ = "";
    }
}
void Node::HandleWrite(const std::string& source) {
    Message outbound_message{MessageType::WriteAck, {}, ""};
    std::string serialized_outbound_message = Message::Serialize(outbound_message);
    Socket::TrySendMessage(serialized_outbound_message, cs425::PORT, source, loss_rate_);

    LogMisc("INFO", "Write ACK sent to " + source);
}

void Node::HandleWriteAck(const std::string& source) {
    if (write_sdfsfilename_ == "") {
        return;
    }

    current_num_write_acks_++;
    LogMisc("INFO", "Write ACK received from " + source);

    int min_W = std::min(cs425::W, (int)members_.size());
    if (current_num_write_acks_ < min_W) {
        return;
    }
    Message replica_outbound_message{MessageType::Replicate, {}, write_file_info_};
    std::string replica_serialized_outbound_message = Message::Serialize(replica_outbound_message);
    
    for (const Member& member : write_replica_members_) {
        Socket::TrySendMessage(replica_serialized_outbound_message, cs425::PORT, member.address, loss_rate_);
    }

    Message addfile_outbound_message{MessageType::AddNewFile, write_replica_members_, write_sdfsfilename_};
    std::string addfile_serialized_outbound_message = Message::Serialize(addfile_outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(addfile_serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }

    LogPut(write_replica_members_, write_sdfsfilename_);

    double put_end_time = GetElapsedSeconds();
    LogMisc("PERF PUT", "PUT took " + std::to_string(put_end_time - put_start_time_) + " seconds");
    put_start_time_ = 0;

    write_replica_members_.clear();
    write_sdfsfilename_ = "";
    write_file_info_ = "";
    current_num_write_acks_ = 0;
}

void Node::LogMemberJoinIntroducer(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[JOIN] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Successfully introduced new member to group" << std::endl;
    std::cout << "Added node " << member.id << " to local membership list" << std::endl;
    std::cout << "Forwarding join to successors via INTRODUCE messages" << std::endl;
    PrintMembers();
}

void Node::LogMemberJoinAck(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[JOIN] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Successfully added self to group" << std::endl;
    std::cout << "Added node " << member.id << " to local membership list" << std::endl;
    PrintMembers();
}

void Node::LogMemberJoin(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[JOIN] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Added node " << member.id << " to local membership list" << std::endl;
    std::cout << "Forwarding join to successors via INTRODUCE messages" << std::endl;
    PrintMembers();
}

void Node::LogMemberLeave(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[LEAVE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Node " << member.id << " requested to leave, removing self from group" << std::endl;
    std::cout << "Forwarding leave to successors via LEAVE messages" << std::endl;
    PrintMembers();
}

void Node::LogMemberLeaveForwarded(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[LEAVE_FORWARDED] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Informed node " << member.id << " either left or failed, removing from local membership list" << std::endl;
    std::cout << "Forwarding leave to successors via LEAVE messages" << std::endl;
    PrintMembers();
}

void Node::LogMemberFailure(const Member& member) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[FAILURE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Detected node " << member.id << " as failed, removing from local membership list" << std::endl;
    std::cout << "Forwarding failure to successors via LEAVE messages" << std::endl;
    PrintMembers();
}

void Node::LogPut(const std::vector<Member>& replicas, const std::string& sdfsfile) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    for (Member replica : replicas) {
        std::cout << "[PUT] - " << replica.id  << " - Time = " << GetElapsedSeconds() << "s" << std::endl;
    }
    std::cout << "Putting file: " << sdfsfile << std::endl;
}

void Node::LogReplicate(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[REPLICATE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Replicated file '" << file_name << "'" << std::endl;
}

void Node::LogMemberAddFile(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[ADD_FILE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Added file '" << file_name << "' to local membership list" << std::endl;
    PrintMembers();
}

void Node::LogFileDelete(const std::vector<Member>& replicas, const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    for (Member replica : replicas) {
        std::cout << "[DELETE from " << replica.id  << "] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    }
    std::cout << "Deleted file '" << file_name << "'" << std::endl;
}

void Node::LogDelete(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[DELETE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Deleted file '" << file_name << "'" << std::endl;
}

void Node::LogMemberDeleteFile(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[DELETE_FILE_FROM_MEM_LIST] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Deleted file '" << file_name << "' from local membership list" << std::endl;
    PrintMembers();
}

void Node::LogGet(const std::vector<Member>& members, const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    for (Member member : members) {
        std::cout << "[GET REQUEST sent to " << member.id  << "] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    }
    std::cout << "Getting file: " << file_name << std::endl;
}

void Node::LogGetRequest(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[GET REQUEST] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Received GET request for file: " << file_name << std::endl;
}

void Node::LogGetResponse(const std::string& file_name) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[GET RESPONSE] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Local file written: " << file_name << std::endl;
}

void Node::LogMisc(const std::string& tag, const std::string& message) {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[" << tag << "] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << message << std::endl;
}

void Node::PrintMembers() {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[MEMBERSHIP_LIST] - Time = " << GetElapsedSeconds() << "s" << std::endl;

    if (!is_member_) {
        std::cout << "Not a member of a group" << std::endl;
        return;
    }

    mutex_.lock();

    std::cout << "Membership list:" << std::endl;
    printf("%30s | %15s | %5s | %s\n", "Id", "Address", "Drops", "Files");
    for (const Member& member : members_) {
        printf("%30s | %15s | %5d | %s\n", member.id.c_str(), member.address.c_str(), member.pings_dropped, member.files_present.c_str());
    }

    std::cout << std::endl;

    mutex_.unlock();
}

void Node::PrintSelf() {
    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "[SELF] - Time = " << GetElapsedSeconds() << "s" << std::endl;
    std::cout << "Self's ID: " << self_.id << std::endl;
}

double Node::GetElapsedSeconds() {
    std::chrono::time_point<std::chrono::steady_clock> current_time = std::chrono::steady_clock::now();
    double milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - start_time_).count();

    return milliseconds / 1000.0;
}

void Node::DebugPrintMembers() {
    if (!is_member_) {
        std::cout << "Not a member of a group" << std::endl;
        return;
    }

    mutex_.lock();

    std::vector<Member>::iterator it = std::find(members_.begin(), members_.end(), self_);

    std::cout << "---------------------------------------------------------------" << std::endl;
    std::cout << "Full list:" << std::endl;
    printf("%30s | %15s | %5s | %s\n", "Id", "Address", "Drops", "Files");
    unsigned int index = 0;
    for (const Member& member : members_) {
        printf("%30s | %15s | %5d | %s", member.id.c_str(), member.address.c_str(), member.pings_dropped, member.files_present.c_str());

        if (index == ring_position_ && it != members_.end()) {
            printf(" <-- rp");
        }

        printf("\n");
        ++index;
    }

    std::cout << "Self + successors:" << std::endl;
    
    // unlikely to trigger
    if (it == members_.end()) {
        std::cout << "(Self not in group)" << std::endl;
    }

    printf("%30s | %15s | %10s\n", "Id", "Address", "Drops");
    if (it != members_.end()) {
        printf("%30s | %15s | %5d (self)\n", it->id.c_str(), it->address.c_str(), it->pings_dropped);
    }

    mutex_.unlock();

    for (const Member& member : GetSuccessors()) {
        printf("%30s | %15s | %5d (successor)\n", member.id.c_str(), member.address.c_str(), member.pings_dropped);
    }

    std::cout << std::endl;
}

std::vector<Member> Node::GetSuccessors() {
    std::vector<Member> successors;
    mutex_.lock();

    if (members_.empty()) {
        mutex_.unlock();
        return {};
    }

    if (std::find(members_.begin(), members_.end(), self_) == members_.end()) {
        unsigned int index = 0;
        while (index < cs425::NUM_MONITORS && index < members_.size()) {
            successors.push_back(members_.at(index));
            ++index;
        }
    } else {
        unsigned int offset = 1;
        // Loop until we hit the number of monitors or we arrive at self
        while (offset <= cs425::NUM_MONITORS && (ring_position_ + offset) % members_.size() != ring_position_) {
            unsigned int index = (ring_position_ + offset) % members_.size();
            successors.push_back(members_.at(index));

            ++offset;
        }
    }

    mutex_.unlock();

    return successors;
}

std::map<std::string, std::vector<Member>> Node::GetFileToMembersMap() {
    std::map<std::string, std::vector<Member>> file_to_members_map;

    for (const Member& member : members_) {
        std::vector<std::string> files = Message::Split(member.files_present, ',');
        for (const std::string& file : files) {
            if (file == "") {
                continue;
            }

            if (file_to_members_map.find(file) == file_to_members_map.end()) {
                file_to_members_map[file] = std::vector<Member>();
            }

            file_to_members_map[file].push_back(member);
        }
    }

    return file_to_members_map;
}

void Node::ReplicateFilesOnLeave(const std::string& file_name, const Member& member_without_file) {
    LogMisc("INFO", "Replicating file " + file_name + " to " + member_without_file.id);

    // iterate through versions, generate file_info, send replicate to member
    std::set<std::string> file_names_versions;
    DIR* dirp;
    struct dirent* direntp;
    dirp = opendir(("./" + file_name).c_str());
    if( dirp != NULL ) {
        for(;;) {
            direntp = readdir( dirp );
            if( direntp == NULL ) break;
            if(strcmp(direntp->d_name, ".")==0 || strcmp(direntp->d_name, "..")==0)
                continue;

            file_names_versions.insert(direntp->d_name);
        }

        closedir( dirp );
    }

    for (const std::string& file_name_version : file_names_versions) {
        std::string file_info = file_name_version + "\n";

        std::ifstream localfile("./" + file_name + "/" + file_name_version);
        if (localfile.is_open()) {
            std::string line;
            while (std::getline(localfile, line)) {
                file_info += line + "\n";
            }
            localfile.close();
        } else {
            LogMisc("ERROR", "Could not open file '" + file_name_version + "\n");
            return;
        }

        Message replica_outbound_message{MessageType::Replicate, {}, file_info};
        std::string replica_serialized_outbound_message = Message::Serialize(replica_outbound_message);
        Socket::TrySendMessage(replica_serialized_outbound_message, cs425::PORT, member_without_file.address, loss_rate_);
    }

    Message addfile_outbound_message{MessageType::AddNewFile, {member_without_file}, file_name};
    std::string addfile_serialized_outbound_message = Message::Serialize(addfile_outbound_message);
    for (const Member& successor : GetSuccessors()) {
        Socket::TrySendMessage(addfile_serialized_outbound_message, cs425::PORT, successor.address, loss_rate_);
    }
}
