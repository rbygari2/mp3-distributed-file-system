#!/bin/bash

for i in 1 2 3 4 5 6 7 8 9 10
do
  ssh -i ~/.ssh/id_ed25519 -t arpitha2@fa22-cs425-34$(printf %02d $i).cs.illinois.edu "
    cd ~/mp3-distributed-file-system; git pull origin main
  "
done