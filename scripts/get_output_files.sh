#!/bin/bash

for i in 1 2 3 4 5 6 7 8 9 10
do
  ssh -t arpitha2@fa22-cs425-34$(printf %02d $i).cs.illinois.edu "
    cat /home/arpitha2/mp3-distributed-file-system/outputs/output.txt 
  " > outputs/output_$i.txt
done